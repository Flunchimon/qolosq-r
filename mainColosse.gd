extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MOVE = Vector2(0, 0)
const acceleration = 2
var motion = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	motion.y = 0
	motion.x = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	motion = move_and_slide(motion, MOVE)
	motion.x = 0
	if Global.DrapeauDash and not(Global.colossed):
		motion.y += acceleration
		
		


func _on_Area2D_body_entered(_body):
	Global.DrapeauDash = true
	$AnimationPlayer.play("Tremblement")
	$Area2D/CollisionShape2D.set_deferred("disabled",true)
	yield($AnimationPlayer, "animation_finished")
	Global.colossed = false
