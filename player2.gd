extends KinematicBody2D

const JUMP_SPEED = 20000
const GRAVITY = 4600
const WALL = 1200
const GROUND_SPEED = 1000
const AIR_SPEED = 800
const MOVE = Vector2(1, -1)
const VERS_GAUCHE = Vector2(1,1)
const VERS_DROITE = Vector2(-1,1)
const POSITION_VERS_GAUCHE = Vector2(-50,0)
const POSITION_VERS_DROITE = Vector2(50,0)
const HP_MAX = 100
const DURATION_TOUCHE = 3
const DURATION_BLOCK = 0.5
const HIT_SPEED = 85
const Y_HIT = 10
const DURATION_SABLE = 3
const DURATION_ENNEMY = 5
const DURATION_Chute = 5
const FLOOR = Vector2.UP
const SNAP_VECT = Vector2.DOWN
const SNAP_LENGHT = 15.0
const VITESSE_MORT = 3000
const Amortissement_STOP = 50
const Amortissement_Air = 100
const Amortissement_Course = 30
const depart_Course = 50

var snap_vector = SNAP_VECT * SNAP_LENGHT
var speed = GROUND_SPEED
var gravity_speed = GRAVITY
var dash_motion = 0
var air_motion = 0
var motion = Vector2()
var ground_Ray = true
var mur_Ray = false
var ground_Ray1 = null
var ground_Ray2 = null
var mur_Ray1 = null
var mur_Ray2 = null
var Permis_gauche = true
var Permis_droite = true
var hasKey = [false, false]
var state_machine
var current_y
var life = 2
var AttaqueEnCours = false
var Aumur = false
var walljump = false
var saut = false
var compteur_glisse = 0
var InputOK = true
var x = 0
var y_init = 0
var y_touche = 0
var timer_SABLE = 0
var timer_ennemy = 0
var timer_Chute = 0
var solstrict = true
var chute = false

var mort = false
var sautMuralPermis = false
var SabreObtenu = false
var timer_touche = 0
var timer_block = 0
var actuel = 0


func _ready():
	life = Global.lives
	
	sautMuralPermis = Global.DrapeauSautMural
	SabreObtenu = Global.DrapeauSabre
	hasKey[0] = Global.DrapeauGrandeCle
	hasKey[1] = Global.DrapeauCle
	
	ground_Ray1 = get_node("groundRay1")
	ground_Ray2 = get_node("groundRay2")
	mur_Ray1 = get_node("murRay1")
	mur_Ray2 = get_node("murRay2")
	state_machine = $AnimationTree.get("parameters/playback")
	state_machine.start("Repos")


func _process(delta):
	var _current = state_machine.get_current_node()
	ground_Ray = ground_Ray2.is_colliding()  or ground_Ray1.is_colliding()
	solstrict = ground_Ray2.is_colliding()  and ground_Ray1.is_colliding()
	mur_Ray = mur_Ray2.is_colliding()  or mur_Ray1.is_colliding()
	
	if timer_block > 0:
		InputOK = false
		timer_block -= delta
	if timer_block <= 0:
		InputOK = true
		
	if timer_ennemy > 0:
		InputOK = false
		timer_ennemy -= delta
	if timer_ennemy <= 1 and timer_ennemy > 0:
		get_tree().change_scene("res://loseEnnemi.tscn")
	
	if timer_SABLE > 0:
		InputOK = false
		timer_SABLE -= delta
	if timer_SABLE <= 1 and timer_SABLE > 0:
		get_tree().change_scene("res://loseSable.tscn")
		
	if timer_Chute > 0:
		InputOK = false
		timer_Chute -= delta
	if timer_Chute <= 1 and timer_Chute > 0:
		get_tree().change_scene("res://loseChute.tscn")
	
	if timer_touche > 0 and not(mort):
		timer_touche -= delta
		$spritePerso.visible = not($spritePerso.visible)
		
	if timer_touche <= 0 and not(mort):
		$spritePerso.visible = true
	if mur_Ray and sautMuralPermis:
		state_machine.travel("Glissade")
		if not Aumur :
			motion.y = motion.y - 10
		compteur_glisse = compteur_glisse + 1
		if compteur_glisse == 7 :
			Aumur = true
			compteur_glisse = 0
			gravity_speed = WALL
	else :

		gravity_speed = GRAVITY
		Aumur = false

	if ground_Ray:
		print(motion.x)
		if chute:
			mort = true
			InputOK = false
			chute = false
			timer_Chute = DURATION_Chute + 1
		if actuel > GROUND_SPEED + Amortissement_Course:
			speed = abs(actuel - Amortissement_Course)
			actuel = abs(motion.x)
		elif actuel < GROUND_SPEED - depart_Course:
			speed = abs(actuel + depart_Course)
			actuel = abs(motion.x)
		else :
			speed = GROUND_SPEED
		actuel = abs(motion.x)
	else :
		if Aumur:
			if motion.y < 0:
				motion.y = motion.y/1.5
		else:
			if actuel > AIR_SPEED:
				speed = abs(actuel - Amortissement_Air)
				actuel = abs(motion.x)
			else :
				speed = AIR_SPEED
			print(motion.x)
			if motion.y < 0:
				if SabreObtenu :
					self.rotation_degrees = 0
					state_machine.travel("Saut Ascensabre")
				else :
					state_machine.travel("Saut Ascendant")
			elif motion.y > 0 and not (mort):
				if SabreObtenu:
					state_machine.travel("Saut Descensabre")
				else :
					state_machine.travel("Saut Descendant")
			else :
				if SabreObtenu :
					state_machine.travel("ReposSabre")
				else :
					state_machine.travel("Repos")
		
	if timer_SABLE > 0:
		motion.y = 50
	else:
		motion.y += gravity_speed * delta
	if AttaqueEnCours:
		motion.x = 0
		motion.y = 0
	elif(dash_motion != 0):
		state_machine.travel("Dash")
		motion.x = dash_motion
		motion.y = 0
	elif air_motion != 0:
		motion.x = air_motion
	elif Input.is_action_pressed("ui_right") and Permis_droite and InputOK:
		motion.x = speed
		$spritePerso.scale=VERS_DROITE
		$spritePerso.position = POSITION_VERS_DROITE
		if ground_Ray:
			if SabreObtenu :
				state_machine.travel("CourseSabre")
			else :
				state_machine.travel("Course")
			
	elif Input.is_action_pressed("ui_left") and Permis_gauche and InputOK:
		motion.x = -speed
		$spritePerso.scale=VERS_GAUCHE
		$spritePerso.position = POSITION_VERS_GAUCHE
		if ground_Ray:
			if SabreObtenu :
				state_machine.travel("CourseSabre")
			else :
				state_machine.travel("Course")
	elif timer_block > 0 and not(mort):
		y_touche = y_init - Y_HIT
		$capacities/jump.saut = true
		if self.position.y < y_touche:
			$capacities/jump.saut = false
		if $spritePerso.scale==VERS_DROITE:
			motion.x = -GROUND_SPEED * sqrt(timer_block)
		else :
			motion.x = GROUND_SPEED * sqrt(timer_block)
	else:
		actuel = abs(motion.x)
		if actuel > 29 :
			if motion.x > 0 :
				motion.x = actuel - Amortissement_STOP
			else : 
				motion.x = - actuel + Amortissement_STOP
		else:
			motion.x = 0
		if abs(motion.x) < 300 :
			if ground_Ray and not(mort):
				if SabreObtenu :
					state_machine.travel("ReposSabre")
				else :
					state_machine.travel("Repos")
			elif timer_SABLE > 0:
				state_machine.travel("Atterissage")
			elif timer_ennemy > 0 and ground_Ray:
				state_machine.travel("Glissade")
				$collisionCourse.set_deferred("disabled",true)
				motion.x = 0
				motion.y = 0

	motion = move_and_slide_with_snap(motion,snap_vector, Vector2.UP, true)
	if motion.y > VITESSE_MORT:
		chute = true


func _on_airePourObjet_area_entered(area):
	if area.is_in_group("grandeCle"):
		hasKey[0] = true
		Global.DrapeauGrandeCle = true
	if area.is_in_group("Timer"):
		pass
		#Global.LeTemps += 15
	if area.is_in_group("cle"):
		hasKey[1] = true
		Global.DrapeauCle = true
	if area.is_in_group("Over"):
		InputOK = false
		var tV = Timer.new()
		tV.set_wait_time(3)
		tV.set_one_shot(true)
		self.add_child(tV)
		tV.start()
		yield(tV, "timeout")
		tV.queue_free()
		get_tree().change_scene("res://loseVide.tscn")
	if area.is_in_group("Sable"):
		mort = true
		InputOK = false
		timer_SABLE = DURATION_SABLE + 1
	if area.is_in_group("nocif"):
		if timer_touche <=0 :
			y_init = self.position.y
			life -= 1
			Global.lives -=1
			timer_block = DURATION_BLOCK
			timer_touche = DURATION_TOUCHE

		if life <= 0 : 
			InputOK = false
			mort = true
			timer_ennemy = DURATION_ENNEMY + 1



func _on_Area2D_body_entered(body):
	self.rotation_degrees = body.rotation_degrees
	if body.name == "escalier":
		self.rotation_degrees = 34.7




