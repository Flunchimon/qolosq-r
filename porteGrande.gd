extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_porteGrande_body_entered(body):
	if body.name == "player" and Global.DrapeauGrandeCle:
		$Serrure.play()
		yield($Serrure,"finished")
		# entrer le nom de la nouvelle scene
		get_tree().change_scene("res://environnementSalleFinale.tscn")
