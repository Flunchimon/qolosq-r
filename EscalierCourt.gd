extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MOVE = Vector2(1, -1)
var motion = Vector2()
var action = false
# Called when the node enters the scene tree for the first time.
func _ready():
	motion.y = 0
	motion.x = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	motion = move_and_slide(motion, MOVE)

func _on_Area2D_area_entered(area):
	if area.is_in_group("player") and not(action):
		action = true
		$AnimatedSprite.play("Chute")
		$AnimatedSprite2.play("Chute")
		$AnimatedSprite3.play("Chute")
		$AnimatedSprite4.play("Chute")
		$AnimatedSprite5.play("Chute")
		$AnimatedSprite6.play("Chute")
		$AnimatedSprite7.play("Chute")
		$AnimatedSprite8.play("Chute")
		$AnimatedSprite9.play("Chute")
		$AnimatedSprite10.play("Chute")
		$AnimatedSprite11.play("Chute")
		$AnimatedSprite12.play("Chute")
		$AnimatedSprite13.play("Chute")
		$AnimatedSprite14.play("Chute")
		$AnimatedSprite15.play("Chute")
		$AnimatedSprite16.play("Chute")
		$AnimatedSprite17.play("Chute")
		$AnimatedSprite18.play("Chute")
		$AnimatedSprite19.play("Chute")
		$AnimatedSprite20.play("Chute")
		$AnimatedSprite21.play("Chute")
		$AnimatedSprite26.play("Chute")
		
		var t = Timer.new()
		t.set_wait_time(0.8)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
		$CollisionShape2D.set_deferred("disabled",true)
		yield($AnimatedSprite, "animation_finished")
		$AnimatedSprite.play("Attente")
		$AnimatedSprite2.play("Attente")
		$AnimatedSprite3.play("Attente")
		$AnimatedSprite4.play("Attente")
		$AnimatedSprite5.play("Attente")
		$AnimatedSprite6.play("Attente")
		$AnimatedSprite7.play("Attente")
		$AnimatedSprite8.play("Attente")
		$AnimatedSprite9.play("Attente")
		$AnimatedSprite10.play("Attente")
		$AnimatedSprite11.play("Attente")
		$AnimatedSprite12.play("Attente")
		$AnimatedSprite13.play("Attente")
		$AnimatedSprite14.play("Attente")
		$AnimatedSprite15.play("Attente")
		$AnimatedSprite16.play("Attente")
		$AnimatedSprite17.play("Attente")
		$AnimatedSprite18.play("Attente")
		$AnimatedSprite19.play("Attente")
		$AnimatedSprite20.play("Attente")
		$AnimatedSprite21.play("Attente")
		$AnimatedSprite26.play("Attente")
		var t2 = Timer.new()
		t2.set_wait_time(5)
		t2.set_one_shot(true)
		self.add_child(t2)
		t2.start()
		yield(t2, "timeout")
		t2.queue_free()
		$CollisionShape2D.set_deferred("disabled",false)
		$AnimatedSprite.play("Idle")
		$AnimatedSprite2.play("Idle")
		$AnimatedSprite3.play("Idle")
		$AnimatedSprite4.play("Idle")
		$AnimatedSprite5.play("Idle")
		$AnimatedSprite6.play("Idle")
		$AnimatedSprite7.play("Idle")
		$AnimatedSprite8.play("Idle")
		$AnimatedSprite9.play("Idle")
		$AnimatedSprite10.play("Idle")
		$AnimatedSprite11.play("Idle")
		$AnimatedSprite12.play("Idle")
		$AnimatedSprite13.play("Idle")
		$AnimatedSprite14.play("Idle")
		$AnimatedSprite15.play("Idle")
		$AnimatedSprite16.play("Idle")
		$AnimatedSprite17.play("Idle")
		$AnimatedSprite18.play("Idle")
		$AnimatedSprite19.play("Idle")
		$AnimatedSprite20.play("Idle")
		$AnimatedSprite21.play("Idle")
		$AnimatedSprite26.play("Idle")
		action = false
		#queue_free()
