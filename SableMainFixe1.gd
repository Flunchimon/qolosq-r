extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var direction = -1
var my_random_number
var rng = RandomNumberGenerator.new()
var compte = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	my_random_number = rng.randi_range (0, 6)
	self.set_frame(my_random_number)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Global.DrapeauDash:
		self.play("whoosh")
		self.position.x += direction * 0
	if self.get_frame() == 7:
		compte += 1

		if compte == 2 :
			queue_free()
		else : 
			my_random_number = rng.randi_range (0, 100)
			self.set_frame(my_random_number%7)
			self.playing = true



