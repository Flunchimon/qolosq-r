extends KinematicBody2D

const JUMP_SPEED = 20000
const GRAVITY = 4600
const WALL = 1200
const GROUND_SPEED = 1000
const AIR_SPEED = 800
const MOVE = Vector2(1, -1)
const VERS_GAUCHE = Vector2(1,1)
const VERS_DROITE = Vector2(-1,1)
const POSITION_VERS_GAUCHE = Vector2(-50,0)
const POSITION_VERS_DROITE = Vector2(50,0)
const HP_MAX = 100
const DURATION_TOUCHE = 3
const DURATION_BLOCK = 0.5
const HIT_SPEED = 85
const Y_HIT = 10

var speed = GROUND_SPEED
var gravity_speed = GRAVITY
var dash_motion = 0
var air_motion = 0
var motion = Vector2()
var ground_Ray = true
var mur_Ray = false
var ground_Ray1 = null
var ground_Ray2 = null
var mur_Ray1 = null
var mur_Ray2 = null
var Permis_gauche = true
var Permis_droite = true
var hasKey = [false, false]
var state_machine
var current_y
var life = 2
var AttaqueEnCours = false
var Aumur = false
var walljump = false
var saut = false
var compteur_glisse = 0
var InputOK = true
var x = 0
var y_init = 0
var y_touche = 0

var sautMuralPermis = false
var SabreObtenu = false
var timer_touche = 0
var timer_block = 0


func _ready():
	life = Global.lives
	$capacities/jump.sautPermis = true
	$capacities/Dash.dashPermis = true
	sautMuralPermis = true
	SabreObtenu = true
	hasKey[0] = false
	hasKey[1] = false
	
	ground_Ray1 = get_node("groundRay1")
	ground_Ray2 = get_node("groundRay2")
	mur_Ray1 = get_node("murRay1")
	mur_Ray2 = get_node("murRay2")
	state_machine = $AnimationTree.get("parameters/playback")
	state_machine.start("Repos")


func _process(delta):
	var _current = state_machine.get_current_node()
	ground_Ray = ground_Ray2.is_colliding()  or ground_Ray1.is_colliding()
	mur_Ray = mur_Ray2.is_colliding()  or mur_Ray1.is_colliding()
	
	if timer_block > 0:
		InputOK = false
		timer_block -= delta
	if timer_block <= 0:
		InputOK = true
	
	if timer_touche > 0:
		timer_touche -= delta
		$spritePerso.visible = not($spritePerso.visible)
		
	if timer_touche <= 0:
		$spritePerso.visible = true
	if mur_Ray and sautMuralPermis:
		state_machine.travel("Glissade")
		if not Aumur :
			motion.y = motion.y - 10
		compteur_glisse = compteur_glisse + 1
		if compteur_glisse == 7 :
			Aumur = true
			compteur_glisse = 0
			gravity_speed = WALL
		
	else :
		gravity_speed = GRAVITY
		Aumur = false
		
	if ground_Ray:
		speed = GROUND_SPEED
	else :
		if Aumur:
			if motion.y < 0:
				motion.y = motion.y/1.5
		else:
			speed = AIR_SPEED
			if motion.y <0:
				if SabreObtenu :
					state_machine.travel("Saut Ascensabre")
				else :
					state_machine.travel("Saut Ascendant")
			elif motion.y >0:
				if SabreObtenu :
					state_machine.travel("Saut Descensabre")
				else :
					state_machine.travel("Saut Descendant")
			else :
				if SabreObtenu :
					state_machine.travel("ReposSabre")
				else :
					state_machine.travel("Repos")
		
		
	motion.y += gravity_speed * delta
	if AttaqueEnCours:
		motion.x = 0
		motion.y = 0
	elif(dash_motion != 0):
		state_machine.travel("Dash")
		motion.x = dash_motion
		motion.y = 0
	elif air_motion != 0:
		motion.x = air_motion
	elif Input.is_action_pressed("ui_right") and Permis_droite and InputOK:
		motion.x = speed
		$spritePerso.scale=VERS_DROITE
		$spritePerso.position = POSITION_VERS_DROITE
		if ground_Ray:
			if SabreObtenu :
				state_machine.travel("CourseSabre")
			else :
				state_machine.travel("Course")
			
	elif Input.is_action_pressed("ui_left") and Permis_gauche and InputOK:
		motion.x = -speed
		$spritePerso.scale=VERS_GAUCHE
		$spritePerso.position = POSITION_VERS_GAUCHE
		if ground_Ray:
			if SabreObtenu :
				state_machine.travel("CourseSabre")
			else :
				state_machine.travel("Course")
	elif timer_block > 0:
		y_touche = y_init - Y_HIT
		$capacities/jump.saut = true
		if self.position.y < y_touche:
			$capacities/jump.saut = false
		if $spritePerso.scale==VERS_DROITE:
			motion.x = -GROUND_SPEED * sqrt(timer_block)
		else :
			motion.x = GROUND_SPEED * sqrt(timer_block)
	else:
		motion.x = 0
		if ground_Ray:
			if SabreObtenu :
				state_machine.travel("ReposSabre")
			else :
				state_machine.travel("Repos")

	motion = move_and_slide(motion,MOVE)


func _on_airePourObjet_area_entered(area):
	if area.is_in_group("grandeCle"):
		hasKey[0] = true
		Global.DrapeauGrandeCle = true
	if area.is_in_group("Timer"):
		pass
		#Global.LeTemps += 15
	if area.is_in_group("cle"):
		hasKey[1] = true
		Global.DrapeauCle = true
	if area.is_in_group("Over"):
		get_tree().change_scene("res://loseVide.tscn")
	if area.is_in_group("Sable"):
		get_tree().change_scene("res://loseSable.tscn")
	if area.is_in_group("nocif"):
		if timer_touche <=0 :
			y_init = self.position.y
			life -= 1
			Global.lives -=1
			timer_block = DURATION_BLOCK
			timer_touche = DURATION_TOUCHE
		print(Global.lives)
		if life <= 0 : 
			get_tree().change_scene("res://loseEnnemi.tscn")


func _on_EpeeTouche_area_entered(_area):
	pass # Replace with function body.
