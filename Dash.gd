extends Node2D

const DASH_LENGTH = 3000
const duration_dash = 0.3
const cool_down = 0.9
const DELAI_DOUBLE_TOUCHE = 0.25

var player = null
var ground_Ray1 = null
var ground_Ray2 = null
var ground_Ray = true
var lock_dash =  false
var dashPermis = false
var right = 1
var timer = 0
var timer_dash = 0
var InputOK = true
var sons
var timer_droit = DELAI_DOUBLE_TOUCHE
var timer_gauche = DELAI_DOUBLE_TOUCHE
var GoDash = false



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	dashPermis = Global.DrapeauDash
	InputOK = player.InputOK
	ground_Ray = ground_Ray2.is_colliding()  or ground_Ray1.is_colliding()
	if player.saut or not ground_Ray : 
		timer_dash = 0
		timer = 0
	timer_droit += delta
	timer_gauche += delta
	# direction du joueur
	if timer <=0:
		if Input.is_action_just_pressed("ui_right") and InputOK:
			right = 1
			if timer_droit < DELAI_DOUBLE_TOUCHE:
				GoDash = true
			else : 
				timer_droit = 0
		elif Input.is_action_just_pressed("ui_left") and InputOK:
			right = -1
			if timer_gauche < DELAI_DOUBLE_TOUCHE:
				GoDash = true
			else : 
				timer_gauche = 0
	
	# MAJ cool_down
	if(timer > 0):
		timer -= delta
	if(timer_dash > 0):
		timer_dash -= delta
		
	# debloquer le dash a la fin du temps si on releve la pression de touche
	if timer <= 0 and lock_dash:
		lock_dash = false

	# demarrage du dash
	if GoDash and InputOK and not lock_dash and ground_Ray and dashPermis:
		sons.play("Dash")
		timer_dash = duration_dash
		timer = cool_down
		lock_dash = true
	# MAJ de la position
	if(timer_dash > 0):
		player.dash_motion = right*DASH_LENGTH
		player.motion.y = 0
	else :
		player.dash_motion = 0
		GoDash = false
	

func _ready():
	dashPermis = Global.DrapeauDash
	player = get_parent().get_parent()
	ground_Ray1 = player.get_node("groundRay1")
	ground_Ray2 = player.get_node("groundRay2")
	sons = player.get_node("BruitagesPerso")




#func _on_airePourObjet_area_entered(area):
#	if area.is_in_group("dash"):
#		Global.DrapeauDash = true
#		dashPermis = true
#		pass

