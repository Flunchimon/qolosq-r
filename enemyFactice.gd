extends KinematicBody2D

const GRAVITY = 20
const SPEED = 70
const FLOOR = Vector2(0, -1)
const HP_MAX = 100
const SEUIL_LATENCE = 30
const MIN_RANGE = 20
const MAX_RANGE = 50

var HP = HP_MAX
var velocity = Vector2()
var direction = 1
var latence = false
var compte = SEUIL_LATENCE
var mort = false
var rng = RandomNumberGenerator.new()
var touche = false
var tourne = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	tourne = 2
	rng.randomize()
	var my_random_number = rng.randi_range (0, 29)
	$AnimatedSprite.set_frame(my_random_number)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if mort :
		velocity.y = 0
		velocity.x = 0
	else:
		velocity.x = SPEED * direction
		velocity.y += GRAVITY
	
	if tourne <= 0:
		rng.randomize()
		var my_random_number2 = rng.randi_range (MIN_RANGE, MAX_RANGE)
		tourne = my_random_number2
		if my_random_number2%2 == 0:
			direction *= -1
	else :
		tourne -= delta
	
	if compte > SEUIL_LATENCE :
		latence = false
	else :
		compte = compte + 1
	if not(latence):
		if direction == 1:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
		if(HP > 0):
			$AnimatedSprite.play("Justeexister")
		velocity = move_and_slide(velocity, FLOOR)
	
	if is_on_wall() or not $RayCast2D.is_colliding():
		direction *= -1
	$Area2D2/CollisionShape2D.position.x = direction * 60.129
	$RayCast2D.position.x = direction * 38
		
func blesser(value, _pos):
	latence = true
	compte = 0
	HP -= value
	velocity.x = 0
	$Area2D/CollisionShape2D.set_deferred("disabled",true)
	if(HP <= 0):
		mort = true
		$CollisionShape2D.set_deferred("disabled",true)
		$AnimatedSprite.play("Mort")
		velocity.x = 0
		velocity.y = 0
		yield($AnimatedSprite, "animation_finished")
		queue_free()
		

func _on_Area2D_area_entered(area):
	if not(area.is_in_group("ennemi")):
		$CollisionShape2D.set_deferred("disabled",true)
		latence = true
		compte = 0
		HP -= HP_MAX
		velocity.x = 0
		mort = true
		$AnimatedSprite.play("Mort")
		velocity.x = 0
		velocity.y = 0
		yield($AnimatedSprite, "animation_finished")
		queue_free()

func _on_Area2D_body_entered(body):
	if body != self and body.name != "TuilePremierPlan" and ("ennemy" in body.name):
		$CollisionShape2D.set_deferred("disabled",true)
		latence = true
		compte = 0
		HP -= HP_MAX
		velocity.x = 0
		mort = true
		$AnimatedSprite.play("Mort")
		velocity.x = 0
		velocity.y = 0
		yield($AnimatedSprite, "animation_finished")
		queue_free()


func _on_Area2D2_area_entered(area):
	if area.is_in_group("ennemi"):
		direction *= -1
