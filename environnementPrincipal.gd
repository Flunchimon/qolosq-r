extends Node2D


# Declare member variables here. Examples:
# var a = 2
var vieLoad = preload("res://objetAjoutTemps.tscn")
var vieDash = vieLoad.instance()
var vieSabre = vieLoad.instance()

var ennemiLoad = preload("res://enemyFactice.tscn")
var ennemif01 = ennemiLoad.instance()
var ennemif02 = ennemiLoad.instance()
var ennemif03 = ennemiLoad.instance()
var ennemif04 = ennemiLoad.instance()
var ennemif05 = ennemiLoad.instance()
var ennemif06 = ennemiLoad.instance()
var ennemif07 = ennemiLoad.instance()
var ennemif08 = ennemiLoad.instance()

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	GlobalJukeBox.get_node("Effervescente").play()
	Global.DrapeauSautMural = Global.init
	Global.DrapeauSabre = Global.init
	Global.DrapeauSaut = Global.init
	Global.DrapeauDash = Global.init
	Global.DrapeauGrandeCle = Global.init
	Global.DrapeauCle = Global.init
	Global.colossed = true
	Global.colossed2 = true
	Global.LeTemps = 100
	Global.lives = 2


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass


func _on_dashObjet_tree_exited():
	vieDash.position = Vector2(565.268, 711)
	call_deferred("add_child",vieDash)
	
	ennemif01.position = Vector2(-6046.447, -840)
	call_deferred("add_child",ennemif01)
	
	ennemif02.position = Vector2(-5346.447, -840)
	call_deferred("add_child",ennemif02)
	
	ennemif03.position = Vector2(-4746.447, -840)
	call_deferred("add_child",ennemif03)
	
	ennemif04.position = Vector2(-6646.447, -840)
	call_deferred("add_child",ennemif04)
	
	ennemif05.position = Vector2(-6646.447, -436.462)
	call_deferred("add_child",ennemif05)
	
	ennemif06.position = Vector2(-6046.447, -436.462)
	call_deferred("add_child",ennemif06)
	
	ennemif07.position = Vector2(-5346.447, -436.462)
	call_deferred("add_child",ennemif07)
	
	ennemif08.position = Vector2(-4746.447, -436.462)
	call_deferred("add_child",ennemif08)

func _on_sabreObjet_tree_exited():
	vieSabre.position = Vector2(-331.241, 1854.25)
	call_deferred("add_child",vieSabre)
