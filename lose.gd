extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event.is_action_pressed("quit"):
		Global.DrapeauSautMural = false
		Global.DrapeauSabre = false
		Global.DrapeauSaut = false
		Global.DrapeauDash = false
		Global.DrapeauGrandeCle = false
		Global.DrapeauCle = false
		Global.colossed = true
		Global.colossed2 = true
		Global.LeTemps = 100
		Global.lives = 2
		get_tree().change_scene("res://menu.tscn")
	if event.is_action_pressed("restart"):
		Global.DrapeauSautMural = false
		Global.DrapeauSabre = false
		Global.DrapeauSaut = false
		Global.DrapeauDash = false
		Global.DrapeauGrandeCle = false
		Global.DrapeauCle = false
		Global.colossed = true
		Global.colossed2 = true
		Global.LeTemps = 100
		Global.lives = 2
		get_tree().change_scene("res://environnementPrincipal.tscn")
