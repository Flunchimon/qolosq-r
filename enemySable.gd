extends KinematicBody2D

const GRAVITY = 20
const SPEED = 70
const FLOOR = Vector2(0, -1)
const HP_MAX = 200
const SEUIL_LATENCE = 30
const AJOUT_TEMPS = 10
const MIN_RANGE = 20
const MAX_RANGE = 50
const DUREE_BUFFER = 3
const TIME_INVINCIBLE = 0.5 * 0.5

var HP = HP_MAX
var velocity = Vector2()
var direction = 1
var latence = false
var compte = SEUIL_LATENCE
var mort = false
var tourne = 0
var rng = RandomNumberGenerator.new()
var touche = false
var compteur = 0
var buffer = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	var my_random_number = rng.randi_range (0, 29)
	$AnimatedSprite.set_frame(my_random_number)
	#$RayCast2D.set_cast_to(RAY)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if Global.DrapeauSabre:
		queue_free()
	
	if mort :
		velocity.y = 0
		velocity.x = 0
	elif touche :
		compteur +=1
		if compteur > 5:
			velocity.x = -SPEED * direction * 100 / compteur
			#velocity.x = -SPEED * direction * 2 * sqrt(35-compteur)

	else:
		velocity.x = SPEED * direction
		velocity.y += GRAVITY
	if buffer > 0:
		buffer -= delta

		
		
	if tourne <= 0:
		rng.randomize()
		var my_random_number2 = rng.randi_range (MIN_RANGE, MAX_RANGE)
		tourne = my_random_number2
		if my_random_number2%2 == 0:
			direction *= -1
	else :
		tourne -= delta
		
	if compte > SEUIL_LATENCE :
		latence = false
	else :
		compte = compte + 1
	if not(latence):
		if direction == 1:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
		if(HP > 0):
			$AnimatedSprite.play("Justeexister")
	velocity = move_and_slide(velocity, FLOOR)
	
	if is_on_wall() or not $RayCast2D.is_colliding():
		if buffer <=0:
			direction *= -1
			buffer = DUREE_BUFFER
			
		
	$RayCast2D.position.x = direction * 38
		
func blesser(value, pos):
	$BruitagesStreumon.play("OuchVrai")
	$BruitagesStreumon.play("ImpactVrai")
	compteur = 0
	if pos < self.position.x :
		direction = -1
	else :
		direction = 1
	if direction == 1:
		$AnimatedSprite.flip_h = false
	else:
		$AnimatedSprite.flip_h = true
	
	latence = true
	compte = 0
	HP -= value
	$Area2D/CollisionShape2D.set_deferred("disabled",true)
	$AnimatedSprite.play("Touch")
	touche = true
	yield($AnimatedSprite, "animation_finished")
	touche = false
	if(HP <= 0):
		mort = true
		$CollisionShape2D.set_deferred("disabled",true)
		Global.LeTemps += AJOUT_TEMPS
		$Ajout.play()
		if (Global.LeTemps >= 80 and Global.LeTemps <= 90) or (Global.LeTemps >= 10 and Global.LeTemps <= 30) :
			$BruitagesColosse.play("Soulage")
		$AnimatedSprite.play("Mort")
		yield($AnimatedSprite, "animation_finished")
		yield($Ajout, "finished")
		queue_free()
	else :
		var t = Timer.new()
		t.set_wait_time(TIME_INVINCIBLE)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
		$Area2D/CollisionShape2D.set_deferred("disabled",false)

		
