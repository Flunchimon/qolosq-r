extends Camera2D


# Declare member variables here. Examples:
# var a = 2
var player = null

#shaking
export var shake_power = 40
export var shake_time = 1.5
var isShake = false
var curPos
var elapsedtime = 0
var value


# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_parent().get_node("player")
	#shaking
	randomize()
	curPos = offset


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#shaking
	if isShake:
		shake(delta) 
	
	value = Global.LeTemps
	if (value == 30 or value == 70 or value == 10) and not isShake:
		elapsedtime = 0
		isShake = true

func shake(delta):
	if elapsedtime<shake_time:
		offset =  Vector2(randf(), randf()) * shake_power
		elapsedtime += delta
	else:
		isShake = false
		elapsedtime = 0
		offset = curPos     




