extends Node2D

const AJOUT_TEMPS = 10

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _process(_delta):
	pass



func _on_TempsAire_body_entered(body):
	if body.name == "player":
		$Sprite/TempsAire/TempsCollision.set_deferred("disabled",true)
		$Ajout.play()
		Global.LeTemps += AJOUT_TEMPS
		if (Global.LeTemps >= 80 and Global.LeTemps <= 90) or (Global.LeTemps >= 10 and Global.LeTemps <= 30) :
			$BruitagesColosse.play("Soulage")
		$Sprite.animation = "Fin"
		yield($Sprite, "animation_finished")
		yield($Ajout, "finished")
		queue_free()
		
