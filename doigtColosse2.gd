extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MOVE = Vector2(0, -1)
const acceleration = 2
var motion = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	motion.y = 0
	motion.x = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	motion = move_and_slide(motion, MOVE)
	if not(Global.colossed2):
		motion.y += acceleration
