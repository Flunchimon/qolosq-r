extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
#	var EMITTER = get_node("boss")
#	EMITTER.connect("blessure_signal", self, "blessure_reception")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#func blessure_reception():
#	get_node("Sprite").set_modulate(Color(0,0,0))
#	get_node("player").set_modulate(Color(0,0,0))
#	get_node("TileMapPremierPlan").set_modulate(Color(0,0,0))
#	get_node("TextureRect").visible = true

func _on_boss_blessure_signal():
	get_node("Sprite").set_modulate(Color(0,0,0,0.9))
	get_node("player").set_modulate(Color(0,0,0))
	get_node("TileMapPremierPlan").set_modulate(Color(0,0,0))
	get_node("TextureRect").set_modulate(Color(1,1,1,1))
