extends Node2D

const Vitesse_GAUCHE = 50
const Vitesse_DROITE = 10
const TEMPS_STASE_GAUCHE = 3000
const TEMPS_STASE_DROITE = 15000
# Declare member variables here. Examples:
var init_y = 0
var state = "gauching"
var y_min = 0
var y_max = 0
var y = 0
var temps_debut_gauche
var temps_debut_droite
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if state == "gauching":
		temps_debut_gauche = OS.get_ticks_msec() 
		$AnimationPlayer.play("RoulementVersGauche")
		state = "stase_gauche"
		
	if state == "droiting":
		temps_debut_droite = OS.get_ticks_msec() 
		state = "stase_droite"
		$AnimationPlayer.play("RoulementVersDroite")

			
	if state == "stase_gauche":
		if OS.get_ticks_msec() > temps_debut_gauche + TEMPS_STASE_GAUCHE :
			state = "droiting"
		
	if state == "stase_droite":
		if OS.get_ticks_msec() > temps_debut_droite + TEMPS_STASE_DROITE :
			state = "gauching"
		
		
