extends Node2D

const cool_down = 0.8*0.6
const cool_down_air = 0.4*0.6
const begin_hit = 200*0.6
const end_hit = 500*0.6
const begin_hit_air = 100
const end_hit_air = 500
const DOMMAGE = 100

var player = null
var colliderVoulu = null
var lock_hit = false
var timer_cool_down = 0
var lock_hit_air = false
var timer_cool_down_air = 0
var now = -7000
var sabreObtenu = false
var ground_Ray = true
var ground_Ray1 = null
var ground_Ray2 = null
var InputOK = true
var current_pos = 0
var hit_allow = true
var sons
var impact
var listeATK = ["Impact2", "Impact3"]

func _ready():
	sabreObtenu = Global.DrapeauSabre
	player = get_parent().get_parent()
	colliderVoulu = player.get_node("spritePerso/EpeeTouche/CollisionArme")
	ground_Ray1 = player.get_node("groundRay1")
	ground_Ray2 = player.get_node("groundRay2")
	sons = player.get_node("BruitagesPerso")
	impact = player.get_node("Impact")
	
func _physics_process(delta):
	ground_Ray = ground_Ray2.is_colliding()  or ground_Ray1.is_colliding()
	if ground_Ray :
		hit_allow = true
	InputOK = player.InputOK
	if lock_hit:
		if OS.get_ticks_msec() > (now+end_hit):
			colliderVoulu.disabled = true
			
		elif OS.get_ticks_msec() > (now+begin_hit):
			colliderVoulu.disabled = false
	
	elif lock_hit_air:
		if OS.get_ticks_msec() > (now+end_hit_air):
			colliderVoulu.disabled = true
			
		elif OS.get_ticks_msec() > (now+begin_hit_air):
			colliderVoulu.disabled = false
	else:
		colliderVoulu.disabled = true
			
		
	# MAJ cool_down
	if(timer_cool_down > 0):
		timer_cool_down -= delta
		
	if(timer_cool_down_air > 0):
		timer_cool_down_air -= delta

	# debloquer le hit a la fin du temps
	if(timer_cool_down <= 0 and lock_hit):
		lock_hit = false
		player.AttaqueEnCours = false
	
	if(timer_cool_down_air <= 0 and lock_hit_air):
		lock_hit_air = false
		

func _input(event):
	ground_Ray = ground_Ray2.is_colliding()  or ground_Ray1.is_colliding()
	if event.is_action_pressed("ui_accept") and InputOK and not lock_hit and ground_Ray:
		if sabreObtenu :
			#animation attaque
			now = OS.get_ticks_msec()
			lock_hit = true
			player.AttaqueEnCours = true
			timer_cool_down = cool_down
			sons.play("Arme")
			player.state_machine.start("Attaque")
	elif event.is_action_pressed("ui_accept") and InputOK and not lock_hit_air and not ground_Ray and hit_allow:
		if sabreObtenu :
			#animation attaque
			now = OS.get_ticks_msec()
			lock_hit_air = true
			hit_allow = false
			timer_cool_down_air = cool_down_air
			sons.play("Arme")
			player.state_machine.start("AttaqueAir")








func _on_EpeeTouche_area_entered(area):
	if area.is_in_group("ennemi"):
		#if area.is_in_group("nocif"):
		current_pos = player.position.x
		area.get_parent().blesser(DOMMAGE, current_pos)
		var anim = listeATK[randi() % listeATK.size()]
		impact.play(anim)
		yield(impact, "animation_finished")
		impact.play("Vide")

func _on_airePourObjet_area_entered(area):
	if area.is_in_group("attaque"):
		$AudioStreamPlayer2D.play()
		sabreObtenu = true
		player.SabreObtenu = true
		Global.DrapeauSabre = true
