extends KinematicBody2D

const JUMP_SPEED = 20000
const GRAVITY = 4600
const WALL = 1200
const GROUND_SPEED = 1000
const AIR_SPEED = 800
const MOVE = Vector2(1, -1)
const VERS_GAUCHE = Vector2(1,1)
const VERS_DROITE = Vector2(-1,1)
const POSITION_VERS_GAUCHE = Vector2(-50,0)
const POSITION_VERS_DROITE = Vector2(50,0)
const HP_MAX = 100
const DURATION_TOUCHE = 3
const DURATION_BLOCK = 0.5
const HIT_SPEED = 85
const Y_HIT = 10
const DURATION_SABLE = 3
const DURATION_ENNEMY = 5
const DURATION_Chute = 5
const FLOOR = Vector2.UP
const SNAP_VECT = Vector2.DOWN
const SNAP_LENGHT = 15.0
const VITESSE_MORT = 3200
const Amortissement_STOP = 50
const Amortissement_ATK = 100
const Amortissement_Air = 40
const Amortissement_Course = 30
const depart_Course = 50
const depart_Air = 50
const HIT_TIMO = 300
const DELAI_COLLE_MUR = 0.25

var LIMITE = GROUND_SPEED
var snap_vector = SNAP_VECT * SNAP_LENGHT
var speed = GROUND_SPEED
var gravity_speed = GRAVITY
var dash_motion = 0
var air_motion = 0
var motion = Vector2()
var ground_Ray = true
var mur_Ray = false
var ground_Ray1 = null
var ground_Ray2 = null
var mur_Ray1 = null
var mur_Ray2 = null
var Permis_gauche = true
var Permis_droite = true
var hasKey = [false, false]
var state_machine
var current_y
var life = 2
var AttaqueEnCours = false
var Aumur = false
var walljump = false
var saut = false
var compteur_glisse = 0
var InputOK = true
var x = 0
var y_init = 0
var y_touche = 0
var timer_SABLE = 0
var timer_ennemy = 0
var timer_Chute = 0
var solstrict = true
var chute = false
var sons
var mort = false
var sautMuralPermis = false
var SabreObtenu = false
var timer_touche = 0
var timer_block = 0
var actuel = 0
var facteur = 1.5
var angle = 0
var solsable = false
var hit_time
var colleMur = false
var delaicolleMur = 0
var sabload = preload("res://SablePas.tscn")
var compteurSable = 0
var GoDash = false
var mortChute = false
var fin_blessure = false


func _ready():
	sons = get_node("BruitagesPerso")
	
	ground_Ray1 = get_node("groundRay1")
	ground_Ray2 = get_node("groundRay2")
	mur_Ray1 = get_node("murRay1")
	mur_Ray2 = get_node("murRay2")
	state_machine = $AnimationTree.get("parameters/playback")
	state_machine.start("Repos")



func _process(delta):
	if fin_blessure:
		if self.modulate.r < 1:
			self.modulate += Color(3*delta,3*delta,3*delta)
			self.modulate.a = 1
		else :
			self.modulate = Color(1,1,1)
			fin_blessure = false
	
	
	sautMuralPermis = Global.DrapeauSautMural
	SabreObtenu = Global.DrapeauSabre
	hasKey[0] = Global.DrapeauGrandeCle
	hasKey[1] = Global.DrapeauCle
	var _current = state_machine.get_current_node()
	ground_Ray = ground_Ray2.is_colliding()  or ground_Ray1.is_colliding()
	solstrict = ground_Ray2.is_colliding()  and ground_Ray1.is_colliding()
	mur_Ray = mur_Ray2.is_colliding()  or mur_Ray1.is_colliding()
	GoDash = $capacities/Dash.GoDash
	
	if timer_block > 0:
		InputOK = false
		timer_block -= delta
	if timer_block <= 0:
		InputOK = true
		
	if timer_ennemy > 0:
		InputOK = false
		timer_ennemy -= delta
	if timer_ennemy <= 1 and timer_ennemy > 0:
		get_tree().change_scene("res://loseEnnemi.tscn")
	
	if timer_SABLE > 0:
		InputOK = false
		timer_SABLE -= delta
	if timer_SABLE <= 1 and timer_SABLE > 0:
		get_tree().change_scene("res://loseSable.tscn")
		
	if timer_Chute > 0:
		InputOK = false
		timer_Chute -= delta
	if timer_Chute <= 1 and timer_Chute > 0:
		get_tree().change_scene("res://loseChute.tscn")
	
	if timer_touche > 0 and not(mort):
		timer_touche -= delta
		if self.modulate == Color(1,1,1):
			$spritePerso.visible = not($spritePerso.visible)
		else :
			$spritePerso.visible = true
		
	if timer_touche <= 0 and not(mort):
		$spritePerso.visible = true
	if mur_Ray and sautMuralPermis and (Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		colleMur = true
		delaicolleMur = DELAI_COLLE_MUR
		state_machine.travel("Glissade")
		if not Aumur :
			motion.y = motion.y - 10
		compteur_glisse = compteur_glisse + 1
		if compteur_glisse == 7 :
			Aumur = true
			compteur_glisse = 0
			gravity_speed = WALL
	else :
		if delaicolleMur >= 0:
			delaicolleMur -= delta
		gravity_speed = GRAVITY
		Aumur = false
	
	if delaicolleMur <= 0:
		colleMur = false
	
	if ground_Ray:
		$BruitagesGlisseMur.stop()
		if motion.x == 0:
			compteurSable = 14
			$BruitagesMarcheSable.stop()
		else:
			if solsable :
				$BruitagesMarcheSable.play()
				if abs(motion.x)>=500:
					compteurSable += 1
				if compteurSable >= 7:
					var sable = sabload.instance()
					get_parent().call_deferred("add_child",sable)
					sable.position.y = self.position.y + 55
					sable.position.x = self.position.x
					compteurSable = 0
			else :
				compteurSable = 14
				$BruitagesMarcheSable.stop()
		LIMITE = GROUND_SPEED
		actuel = abs(motion.x)
		if chute:
			mort = true
			mortChute = true
			InputOK = false
			chute = false
			timer_Chute = DURATION_Chute + 1
		if actuel > GROUND_SPEED + depart_Course :
			speed = Amortissement_Course
		else:
			speed = depart_Course
	else :
		$BruitagesMarcheSable.stop()
		if Aumur:
			$BruitagesGlisseMur.play()
			if motion.y < 0:
				motion.y = motion.y/1.5
		else:
			$BruitagesGlisseMur.stop()
			actuel = abs(motion.x)
			LIMITE = AIR_SPEED
			if actuel > AIR_SPEED + depart_Air:
				speed = Amortissement_Air
			else :
				speed = depart_Air
			if motion.y < 0:
				if InputOK :
					if SabreObtenu:
						self.rotation_degrees = 0
						state_machine.travel("Saut Ascensabre")
					else :
						state_machine.travel("Saut Ascendant")
				else :
					if SabreObtenu:
						state_machine.travel("BlessureSabre")
					else :
						state_machine.travel("Blessure")
			elif motion.y > 0 and not (mort):
				if InputOK :
					if SabreObtenu:
						state_machine.travel("Saut Descensabre")
					else :
						state_machine.travel("Saut Descendant")
				else :
					if SabreObtenu:
						state_machine.travel("BlessureSabre")
					else :
						state_machine.travel("Blessure")
			else :
				if SabreObtenu :
					state_machine.travel("ReposSabre")
				else :
					state_machine.travel("Repos")
					
	if timer_SABLE > 0:
		motion.y = 50
	else:
		motion.y += gravity_speed * delta
	if AttaqueEnCours and ground_Ray:
		if motion.x > 0 :
			motion.x = motion.x - Amortissement_ATK
		elif motion.x < 0 :
			motion.x = motion.x + Amortissement_ATK
		else :
			motion.x = 0
	elif(dash_motion != 0):
		state_machine.travel("Dash")
		motion.x = dash_motion
		motion.y = 0
	elif air_motion != 0:
		motion.x = air_motion
	elif Input.is_action_pressed("ui_right") and Permis_droite and InputOK:
		if motion.x < - LIMITE :
			motion.x = motion.x + 3 * speed
		elif motion.x < LIMITE :
			motion.x = motion.x + speed
		else:
			motion.x = motion.x - speed
		$spritePerso.scale=VERS_DROITE
		$spritePerso.position = POSITION_VERS_DROITE
		if ground_Ray:
			if SabreObtenu :
				state_machine.travel("CourseSabre")
			else :
				state_machine.travel("Course")
			
	elif Input.is_action_pressed("ui_left") and Permis_gauche and InputOK:
		if motion.x > LIMITE :
			motion.x = motion.x - 3 * speed
		elif motion.x > - LIMITE :
			motion.x = motion.x - speed
		else:
			motion.x = motion.x + speed
		$spritePerso.scale=VERS_GAUCHE
		$spritePerso.position = POSITION_VERS_GAUCHE
		if ground_Ray:
			if SabreObtenu :
				state_machine.travel("CourseSabre")
			else :
				state_machine.travel("Course")
	elif timer_block > DURATION_BLOCK - 0.1 and not(mort):
		
		y_touche = y_init - Y_HIT
		$capacities/jump.saut = true
		if self.position.y < y_touche or (OS.get_ticks_msec()> hit_time + HIT_TIMO) :
			$capacities/jump.saut = false
		if $spritePerso.scale==VERS_DROITE:
			motion.x = -GROUND_SPEED * sqrt(timer_block)
		else :
			motion.x = GROUND_SPEED * sqrt(timer_block)
	else:
		actuel = abs(motion.x)
		if actuel > 29 :
			if motion.x > 0 :
				motion.x = motion.x - Amortissement_STOP
			else : 
				motion.x = motion.x + Amortissement_STOP
		else:
			motion.x = 0
		if abs(motion.x) < 300 :
			if ground_Ray and not(mort):
				if SabreObtenu :
					state_machine.travel("ReposSabre")
				else :
					state_machine.travel("Repos")
			elif timer_SABLE > 0:
				state_machine.travel("Atterissage")
			elif timer_ennemy > 0 and ground_Ray:

				state_machine.travel("Pisabrenacle")
				$collisionCourse.set_deferred("disabled",true)
				motion.x = 0
				motion.y = 0

	motion = move_and_slide_with_snap(motion,snap_vector, Vector2.UP, true)
	if motion.y > VITESSE_MORT:
		chute = true
	$Impact.scale.x = -$spritePerso.scale.x
	$Impact.position.x = -$spritePerso.scale.x * 111.034
	
	
	if mort:
		if mortChute:
			if SabreObtenu :
				state_machine.travel("MortChuteSabre")
			else :
				state_machine.travel("MortChute")
		else:
			motion.y += speed * 1.5
			$Course.set_deferred("disabled", true)
			if SabreObtenu :
				state_machine.travel("MortSabre")
			else :
				state_machine.travel("Mort")


func _on_airePourObjet_area_entered(area):
	if area.is_in_group("grandeCle"):
		hasKey[0] = true
		Global.DrapeauGrandeCle = true
	if area.is_in_group("Timer"):
		pass
		#Global.LeTemps += 15
	if area.is_in_group("cle"):
		hasKey[1] = true
		Global.DrapeauCle = true
	if area.is_in_group("Over"):
		InputOK = false
		var tV = Timer.new()
		tV.set_wait_time(3)
		tV.set_one_shot(true)
		self.add_child(tV)
		tV.start()
		yield(tV, "timeout")
		tV.queue_free()
		get_tree().change_scene("res://loseVide.tscn")
	if area.is_in_group("Sable"):
		mort = true
		InputOK = false
		timer_SABLE = DURATION_SABLE + 1
	if area.is_in_group("nocif"):
		if timer_touche <=0 :
			sons.play("PersoTouche")
			y_init = self.position.y
			hit_time = OS.get_ticks_msec()
			Global.lives -=1
			timer_block = DURATION_BLOCK
			timer_touche = DURATION_TOUCHE

		if Global.lives <= 0 : 
			InputOK = false
			mort = true
			
			timer_ennemy = DURATION_ENNEMY + 1



func _on_Area2D_body_entered(body):
	
#	if body.name == "escalier":
#		angle = 34.7
#		facteur = 1
#	elif body.name == "player" :
#		pass
#	else:
#		angle = -body.rotation_degrees
#		facteur = 3.5
#	self.rotation_degrees = angle / facteur
	
	if body.is_in_group("Sablax"):
		solsable = true
		$Course.set_deferred("disabled", true)



func _on_Area2D_body_exited(body):
	
	if body.is_in_group("Sablax"):
		solsable = false
		$Course.set_deferred("disabled", false)



func _on_boss_finblessure_signal():
	fin_blessure = true
