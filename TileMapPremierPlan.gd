extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var fin_blessure = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if fin_blessure:
		if self.modulate.r < 1:
			self.modulate += Color(3*delta,3*delta,3*delta)
			self.modulate.a = 1
		else :
			self.modulate = Color(1,1,1)
			fin_blessure = false


func _on_boss_finblessure_signal():
	fin_blessure = true
