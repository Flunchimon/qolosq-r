extends TextureRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var fin_blessure = false

# Called when the node enters the scene tree for the first time.
func _ready():
	modulate.a = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if fin_blessure:
		modulate.a -= 3*delta
		if modulate.a <= 0:
			fin_blessure = false


func _on_boss_finblessure_signal():
	fin_blessure = true
