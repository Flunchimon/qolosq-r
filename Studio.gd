extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var timer

# Called when the node enters the scene tree for the first time.
func _ready():
	timer = 10


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if timer>0:
		timer = timer-delta
	else :
		get_tree().change_scene("res://menu.tscn")
