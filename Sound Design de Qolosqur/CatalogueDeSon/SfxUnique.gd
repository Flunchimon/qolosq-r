extends Node2D

var playing
var sound
var run

func _ready():
	randomize()
	
func _process(_delta):
	if run:
		playing = sound.playing

func play():
	if not(playing):
		run = true
		var c = randi()%get_child_count()
		sound = get_child(c)
		sound.play()
	
func stop():
	if playing:
		sound.stop()
