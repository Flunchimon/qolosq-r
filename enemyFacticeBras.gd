extends KinematicBody2D

const GRAVITY = 20
const SPEED = 70
const FLOOR = Vector2(0, -1)
const HP_MAX = 100
const SEUIL_LATENCE = 30

var HP = HP_MAX
var velocity = Vector2()
var direction = 1
var latence = false
var compte = SEUIL_LATENCE
var mort = false
var rng = RandomNumberGenerator.new()
var touche = false

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	var my_random_number = rng.randi_range (0, 29)
	$AnimatedSprite.set_frame(my_random_number)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	if Global.DrapeauDash:
		queue_free()
	
	if mort :
		velocity.y = 0
		velocity.x = 0
	else:
		velocity.x = SPEED * direction
		velocity.y += GRAVITY
	
	
	if compte > SEUIL_LATENCE :
		latence = false
	else :
		compte = compte + 1
	if not(latence):
		if direction == 1:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
		if(HP > 0):
			$AnimatedSprite.play("Justeexister")
		velocity = move_and_slide(velocity, FLOOR)
	
	if is_on_wall() or not $RayCast2D.is_colliding():
		direction *= -1
	
	$RayCast2D.position.x = direction * 38
		
func blesser(value, pos):
	latence = true
	compte = 0
	HP -= value
	velocity.x = 0
	$Area2D/CollisionShape2D.set_deferred("disabled",true)
	if(HP <= 0):
		mort = true
		$CollisionShape2D.set_deferred("disabled",true)
		$AnimatedSprite.play("Mort")
		velocity.x = 0
		velocity.y = 0
		yield($AnimatedSprite, "animation_finished")
		queue_free()
		
