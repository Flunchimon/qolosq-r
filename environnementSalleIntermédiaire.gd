extends Node2D

const TOTAL = 6
const TIMER_CAMERA = 4
# Declare member variables here. Examples:
var tues = 0
var cleLoad = preload("res://objetCleSimple.tscn")
var cle = cleLoad.instance()
var timer_cam = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.DrapeauGrandeCle:
		$CanvasLayer.queue_free()



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if timer_cam > 0 :
		$CameraCle.current = true
		timer_cam -= delta
	else :
		$CameraIntermediaire.current = true


func _on_enemy_tree_exited():
	tues += 1
	if tues == TOTAL:
		cle.position = Vector2(447.589, -500)
		call_deferred("add_child",cle)
		timer_cam = TIMER_CAMERA
		
