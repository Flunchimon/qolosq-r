extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MOVE = Vector2(1, -1)
const acceleration = 2
const frequence_clignotement = 7
const seuil_visibilite = 7 * frequence_clignotement
var motion = Vector2()
var compteur = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false
	motion.y = 0
	motion.x = 0
	compteur = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	motion = move_and_slide(motion, MOVE)
	if Global.DrapeauDash:
		if compteur == 0 :
			pass#self.visible = true
		compteur +=1
		if compteur < seuil_visibilite and compteur % frequence_clignotement ==0 :
			pass#self.visible = not(self.visible)
	if Global.DrapeauDash and not(Global.colossed):
		motion.y += acceleration
