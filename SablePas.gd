extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	self.play("whoosh")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if self.get_frame() == 8:
		queue_free()

