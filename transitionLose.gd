extends Control


# Declare member variables here. Examples:
const SEUIL = 50
var compteur = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	GlobalJukeBox.get_node("Effervescente").stop()
	Engine.set_time_scale(1)
	compteur = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	compteur += 1
	if compteur > SEUIL :
		get_tree().change_scene("res://lose.tscn")
