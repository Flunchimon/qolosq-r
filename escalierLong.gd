extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MOVE = Vector2(1, -1)
var motion = Vector2()
var action = false
# Called when the node enters the scene tree for the first time.
func _ready():
	motion.y = 0
	motion.x = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	motion = move_and_slide(motion, MOVE)

func _on_Area2D_area_entered(area):
	if area.is_in_group("player") and not(action):
		action = true
		$AnimatedSprite.play("Chute")
		$AnimatedSprite2.play("Chute")
		$AnimatedSprite3.play("Chute")
		$AnimatedSprite4.play("Chute")
		$AnimatedSprite5.play("Chute")
		$AnimatedSprite6.play("Chute")
		$AnimatedSprite7.play("Chute")
		var t = Timer.new()
		t.set_wait_time(0.8)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
		$CollisionShape2D.set_deferred("disabled",true)
		yield($AnimatedSprite, "animation_finished")
		$AnimatedSprite.play("Attente")
		$AnimatedSprite2.play("Attente")
		$AnimatedSprite3.play("Attente")
		$AnimatedSprite4.play("Attente")
		$AnimatedSprite5.play("Attente")
		$AnimatedSprite6.play("Attente")
		$AnimatedSprite7.play("Attente")
		var t2 = Timer.new()
		t2.set_wait_time(5)
		t2.set_one_shot(true)
		self.add_child(t2)
		t2.start()
		yield(t2, "timeout")
		t2.queue_free()
		$CollisionShape2D.set_deferred("disabled",false)
		$AnimatedSprite.play("Idle")
		$AnimatedSprite2.play("Idle")
		$AnimatedSprite3.play("Idle")
		$AnimatedSprite4.play("Idle")
		$AnimatedSprite5.play("Idle")
		$AnimatedSprite6.play("Idle")
		$AnimatedSprite7.play("Idle")
		action = false
		#queue_free()
