extends Node2D

const JUMP_SPEED = 25000
const TEMPS_SAUT = 100
const LATENCE_SAUT = 300
const MAX_Y = 36
const MIN_Y = 10

var state_machine
var player = null
var TheAnimTree = null
var ground_Ray1 = null
var ground_Ray2 = null
var ground_Ray = true
var saut = false
var y_init = 0
var y_saut = MIN_Y
var sons
var now = 0
var la = 0

var sautPermis = false
var InputOK = true 

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _input(event):
	InputOK = player.InputOK
	sons = player.get_node("BruitagesPerso")
	sautPermis = Global.DrapeauSaut
	ground_Ray = ground_Ray2.is_colliding()  or ground_Ray1.is_colliding()
	la = OS.get_ticks_msec()
	if event.is_action_pressed("ui_up") and ground_Ray and sautPermis and InputOK and not(Global.DrapeauSautMural and player.walljump) and (la > now + LATENCE_SAUT) and not(player.GoDash):
			sons.play("SautRP")
			saut = true
			player.saut = true
			y_init = player.position.y
			now = OS.get_ticks_msec()
			
func _physics_process(delta):
	sautPermis = Global.DrapeauSaut
	if not player.Aumur :
		if saut:
			player.motion.y -= JUMP_SPEED * delta
		
		if Input.is_action_pressed("ui_up"):
			y_saut = y_init - MAX_Y
			la = OS.get_ticks_msec()
			if la > now + TEMPS_SAUT : 
				saut = false
				player.saut = false
		else :
			y_saut = y_init - MIN_Y
			if player.position.y < y_init - MIN_Y :
				saut = false
				player.saut = false
			
		if player.position.y < y_saut:
			saut = false
			player.saut = false
	
func _ready():
	player = get_parent().get_parent()
	ground_Ray1 = player.get_node("groundRay1")
	ground_Ray2 = player.get_node("groundRay2")


func _on_airePourObjet_area_entered(area):
	if area.is_in_group("saut"):
		Global.DrapeauSaut = true
