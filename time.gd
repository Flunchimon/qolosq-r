extends TextureProgress

export(Gradient) var gradient

func _on_value_changed(_new_value):
	tint_progress = gradient.interpolate(ratio)

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start()
	Global.LeTemps -= 1
	value = Global.LeTemps


func _on_Timer_timeout():
	Global.LeTemps -= 1
	value = Global.LeTemps
	if value == 30 or value == 70 or value == 10 :
		$BruitagesColosse.play("Souffre")
	if Global.LeTemps < 0:
		Engine.set_time_scale(0.1)
		var tS = Timer.new()
		tS.set_wait_time(0.3)
		tS.set_one_shot(true)
		self.add_child(tS)
		tS.start()
		yield(tS, "timeout")
		tS.queue_free()
		get_tree().change_scene("res://loseTemps.tscn")
