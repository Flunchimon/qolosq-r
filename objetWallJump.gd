extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_mainAire_body_entered(body):
	if body.name == "player":
		$Sprite/mainAire/mainCollision.set_deferred("disabled",true)
		$Sprite.animation = "apresSaisie"
		$AudioStreamPlayer2D.play()
		yield($Sprite, "animation_finished")
		yield($AudioStreamPlayer2D, "finished")
		queue_free()
