extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if self.position.y < 134:
		self.position.y = self.position.y + 5


func _on_cleSimpleAire_body_entered(body):
	if body.name == "player":
		$Sprite/cleSimpleAire/cleSimpleCollision.set_deferred("disabled",true)
		$AudioStreamPlayer2D.play()
		$Sprite.animation = "apresSaisie"
		yield($Sprite, "animation_finished")
		yield($AudioStreamPlayer2D, "finished")
		Global.DrapeauCle = true
		queue_free()
