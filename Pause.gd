extends Control
var new_pause_state = false

func _input(event):
	if event.is_action_pressed("pause"):
		new_pause_state = not get_tree().paused
		get_tree().paused = new_pause_state
		visible = new_pause_state
	if new_pause_state:
		if event.is_action_pressed("quit"):
			get_tree().paused = not get_tree().paused
			GlobalJukeBox.get_node("Effervescente").stop()
			get_tree().change_scene("res://menu.tscn")
		if event.is_action_pressed("restart"):
			get_tree().paused = not get_tree().paused
			get_tree().change_scene("res://environnementPrincipal.tscn")
	
