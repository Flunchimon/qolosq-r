extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_porteAvecSerrure_body_exited(body):
	if body.name == "player" and Global.DrapeauGrandeCle:
		# entrer le nom de la nouvelle scene
		get_tree().change_scene("res://environnementSalleIntermédiaireVide.tscn")


func _on_porteAvecSerrure_body_entered(body):
	if body.name == "player" and Global.DrapeauGrandeCle:
		var t = Timer.new()
		t.set_wait_time(0.5)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
		get_tree().change_scene("res://environnementSalleIntermédiaireVide.tscn")
