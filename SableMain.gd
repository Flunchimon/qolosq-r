extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var direction = -1
var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var my_random_number = rng.randi_range (0, 6)
	self.set_frame(my_random_number)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Global.DrapeauDash:
		self.play("whoosh")
		self.position.x += direction * 6
	if self.get_frame() == 7:
		queue_free()
		#if my_random_number2 :
		#	queue_free()
		#else : 
		#	self.set_frame(my_random_number2)

