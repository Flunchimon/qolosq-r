extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var direction = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	self.play("whoosh")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	self.position.x -= direction * 5
	if self.get_frame() == 6:
		queue_free()

