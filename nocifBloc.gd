extends Node2D

const Vitesse_Descente = 50
const Vitesse_Montante = 10
const TEMPS_STASE_BASSE = 3000
const TEMPS_STASE_HAUTE = 6000
const TEMPS_ANIMATION = 1000
# Declare member variables here. Examples:
var init_y = 0
var state
var y_min = 0
var y_max = 0
var y = 0
var temps_debut_bas
var temps_debut_haut
# var b = "text"
var sabload = preload("res://SableThwomp.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	y_min = 610
	y_max = 450
	state = "downing"

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	
	y=self.position.y
	
	if state == "downing":
		$AnimatedSprite.play("Chute")
		self.position.y += Vitesse_Descente
		if y>y_min :
			self.position.y = y
			state = "stase_basse"
			temps_debut_bas = OS.get_ticks_msec()
			var sableD = sabload.instance()
			var sableG = sabload.instance()
			call_deferred("add_child",sableG)
			call_deferred("add_child",sableD)
			sableG.position.y = 0
			sableD.position.y = 0
			
			sableG.direction = 1
			sableD.direction = -1
			
		
	if state == "uping":
		$AnimatedSprite.play("Normal")
		self.position.y -= Vitesse_Montante
		if y<y_max :
			self.position.y = y
			temps_debut_haut = OS.get_ticks_msec() 
			state = "stase_haute"
			
			
	if state == "stase_basse":
		$AnimatedSprite.play("Normal")
		if OS.get_ticks_msec() > temps_debut_bas + TEMPS_STASE_BASSE :
			state = "uping"
		
	if state == "stase_haute":
		if OS.get_ticks_msec() > temps_debut_haut + TEMPS_STASE_BASSE - TEMPS_ANIMATION :
			$AnimatedSprite.play("Tremble")
		if OS.get_ticks_msec() > temps_debut_haut + TEMPS_STASE_BASSE :
			state = "downing"
		
		
	pass

