extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_cleGrandeAire_body_entered(body):
	if body.name == "player":
		$Sprite/cleGrandeAire/cleGrandeCollision.set_deferred("disabled",true)
		$AudioStreamPlayer2D.play()
		$Sprite.animation = "apresSaisie"
		yield($AudioStreamPlayer2D, "finished")
		yield($Sprite, "animation_finished")
		queue_free()
