extends Area2D

var time = 3
var canHit = true
var stillIn = false
var player = null

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if canHit and stillIn and player != null:
		player.life -= 1
		canHit = false
		$Timer.start(time)


func _on_pike_body_entered(body):
	if body.name == "player" and canHit:
		body.life -= 1
		player = body
		$Timer.start(time)
		canHit = false
		stillIn = true

func _on_Timer_timeout():
	canHit = true

func _on_pike_body_exited(body):
	stillIn = false
	player = null
