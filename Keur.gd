extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Keur_body_entered(body):
	if body.name == "player":
		var t = Timer.new()
		t.set_wait_time(0.15)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		Engine.set_time_scale(0.25)
		yield(t, "timeout")
		Engine.set_time_scale(1)
		get_tree().change_scene("res://menu.tscn")
