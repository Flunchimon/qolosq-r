extends KinematicBody2D

const GRAVITY = 20
const SPEED = 150
const SPEEDATK = 3000.0
const FLOOR = Vector2(0, -1)
const HP_MAX = 500
const SEUIL_LATENCE = 30
const AJOUT_TEMPS = 10
const MIN_RANGE = 20
const MAX_RANGE = 50
const DUREE_BUFFER = 3
const TIME_INVINCIBLE = 0.5 * 0.25

var HP = HP_MAX
var velocity = Vector2()
var direction = 1
var latence = false
var compte = SEUIL_LATENCE
var mort = false
var attaque = false
var fin_attaque = false
var tourne = 0
var rng = RandomNumberGenerator.new()
var touche = false
var compteur = 0
var buffer = 0
var reboursAttaque = 0
var seuilAttaque
var topAttaque = 0
var fin_blessure = false

signal blessure_signal
signal finblessure_signal

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	var my_random_number = rng.randi_range (0, 29)
	$AnimatedSprite.set_frame(my_random_number)
	attaque = false
	seuilAttaque = rng.randi_range (50, 100)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if fin_blessure:
		if self.modulate.r < 1:
			self.modulate += Color(3*delta,3*delta,3*delta)
			self.modulate.a = 1
		else :
			self.modulate = Color(1,1,1)
			fin_blessure = false
	
	
	
	if fin_attaque:
		attaque = false
		fin_attaque = false
		topAttaque = 0
	
	if mort :
		velocity.y = 0
		velocity.x = 0
	elif touche :
		compteur +=1
		if compteur > 5:
			velocity.x = -SPEED * direction * 25 / compteur
			#velocity.x = -SPEED * direction * 2 * sqrt(35-compteur)
	elif attaque :
		if topAttaque == -1 :
			velocity.x += - SPEEDATK / 50 * direction
		else :
			velocity.y = 0
			velocity.x = SPEEDATK * direction * topAttaque
	else:
		velocity.x = SPEED * direction
		velocity.y += GRAVITY
		
		
	if buffer > 0:
		buffer -= delta
		
		
	if $AnimatedSprite.get_animation() == "Justeexister":
		$Area2D/CollisionAttaque1.set_deferred("disabled",true)
		if $AnimatedSprite.get_frame() == 7:
			reboursAttaque +=1
			if reboursAttaque > seuilAttaque :
				attaque = true
		if not($AnimatedSprite.get_frame() > 0 and $AnimatedSprite.get_frame() < 5) :
			$Area2D/CollisionQueue.set_deferred("disabled",true)
		else :
			$Area2D/CollisionQueue.set_deferred("disabled",false)
	else:
		$Area2D/CollisionQueue.set_deferred("disabled",true)
		
	if $AnimatedSprite.get_animation() == "Attaque":
		if $AnimatedSprite.get_frame() == 21 :
			fin_attaque = true
			seuilAttaque = rng.randi_range (40, 80)
			reboursAttaque = 0
		elif $AnimatedSprite.get_frame() >= 19 :
			topAttaque = 0
		elif $AnimatedSprite.get_frame() == 11:
			topAttaque = 1
		elif $AnimatedSprite.get_frame() >= 17 :
			topAttaque = -1
		elif $AnimatedSprite.get_frame() > 11:
			topAttaque = 0
			$Area2D/CollisionQueue.set_deferred("disabled",false)
			$Area2D/CollisionAttaque1.set_deferred("disabled",false)
		else :
			topAttaque = 0

		
	if tourne <= 0:
		rng.randomize()
		var my_random_number2 = rng.randi_range (MIN_RANGE, MAX_RANGE)
		tourne = my_random_number2
		if my_random_number2%2 == 0:
			direction *= -1
	else :
		tourne -= delta
		
	if compte > SEUIL_LATENCE :
		latence = false
	else :
		compte = compte + 1
		
		
	if not(latence):
		if direction == 1:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
		if(HP > 0):
			if attaque :
				$AnimatedSprite.play("Attaque")
			else:
				$AnimatedSprite.play("Justeexister")
	velocity = move_and_slide(velocity, FLOOR)
	
	if (is_on_wall() or not $RayCast2D.is_colliding()) and not(attaque):
		#if buffer <=0:
		direction *= -1
		buffer = DUREE_BUFFER
	
	$CollisionTete.position.x = direction * 44.593
	$Area2D/CollisionShape2D.position.x = direction * (-11.815)
	$Area2D/CollisionTete.position.x = direction * 20.04
	$Area2D/CollisionQueue.position.x = direction * (-59.239)
	$Area2D/CollisionAttaque1.position.x = direction * 56.754
	$Faible/CollisionShape2D.position.x = direction * (-44.29)
	$CollisionShape2D.position.x = direction * (-12.946)
	
	$RayCast2D.position.x = direction * 38
		
func blesser(value, pos):
	#$BruitagesStreumon.play("OuchVrai")
	#$BruitagesStreumon.play("ImpactVrai")
	emit_signal("blessure_signal")
	compteur = 0
	#get_node("AnimatedSprite").set_modulate(Color(0,0,0))
	self.modulate = Color(0,0,0)
	print(self.modulate)
	if pos < self.position.x :
		direction = -1
	else :
		direction = 1
	if direction == 1:
		$AnimatedSprite.flip_h = false
	else:
		$AnimatedSprite.flip_h = true
	
	latence = true
	compte = 0
	HP -= value
	$Area2D/CollisionShape2D.set_deferred("disabled",true)
	$Area2D/CollisionTete.set_deferred("disabled",true)
	$Area2D/CollisionQueue.set_deferred("disabled",true)
	$AnimatedSprite.play("Touch")
	touche = true
	yield($AnimatedSprite, "animation_finished")
	touche = false
	if(HP <= 0):
		mort = true
		$CollisionShape2D.set_deferred("disabled",true)
		Engine.set_time_scale(0.1)
		$AnimatedSprite.play("Mort")
		yield($AnimatedSprite, "animation_finished")
		Engine.set_time_scale(1)
		get_tree().change_scene("res://environnementSalleGrosseCleAprèsBoss.tscn")
		queue_free()
	else :
		var t = Timer.new()
		t.set_wait_time(TIME_INVINCIBLE)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
		
		fin_blessure = true
		emit_signal("finblessure_signal")
		$Area2D/CollisionShape2D.set_deferred("disabled",false)
		$Area2D/CollisionTete.set_deferred("disabled",false)
		$Area2D/CollisionQueue.set_deferred("disabled",false)



