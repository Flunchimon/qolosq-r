extends Node2D

const AJOUT_VIE = 1

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _process(_delta):
	pass



func _on_TempsAire_body_entered(body):
	if body.name == "player":
		$Sprite/TempsAire/TempsCollision.set_deferred("disabled",true)
		$Ajout.play()
		Global.lives = 2
		$Sprite.animation = "Fin"
		yield($Sprite, "animation_finished")
		yield($Ajout, "finished")
		queue_free()
		
