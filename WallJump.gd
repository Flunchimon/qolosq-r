extends Node2D

const JUMP = 1000
const TEMPS_SAUT = 0.3
const TEMPS_DASH = 0.1
const TIMER_SAUT = 1
const DASH_LENGTH = 300
const MURAL_X = 750
const FACTEUR_ATTENUATION = 0.8
const DELAI_AVANT_MEME = 5


var player = null
var ground_Ray1 = null
var ground_Ray2 = null
var ground_Ray = false
var ground_Ray1_Bool = true
var ground_Ray2_Bool = true
var mur_Ray1 = null
var mur_Ray2 = null
var mur_Ray = false
var mur_Ray1_Bool = false
var mur_Ray2_Bool = false
var sautMuralPermis = false
var temps_saut = 0
var temps_dash = 0
var timer_saut = 0
var right = 1
var PermisGauche = true
var PermisDroite = true
var InputOK = true
var sons
var delai = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _input(event):
	mur_Ray1_Bool = mur_Ray2.is_colliding()
	mur_Ray2_Bool = mur_Ray1.is_colliding()
	mur_Ray = mur_Ray1_Bool or mur_Ray2_Bool
	
	ground_Ray1_Bool = ground_Ray2.is_colliding()
	ground_Ray2_Bool = ground_Ray1.is_colliding()
	ground_Ray = ground_Ray1_Bool or ground_Ray2_Bool
	
	if mur_Ray and not (ground_Ray):
		player.walljump = true
	if event.is_action_pressed("ui_up") and mur_Ray and sautMuralPermis and InputOK and player.colleMur :
		delai = DELAI_AVANT_MEME
		sons.play("SautRP")
		temps_saut = TEMPS_SAUT
		temps_dash = TEMPS_DASH
		timer_saut = TIMER_SAUT
		player.motion.y = -JUMP
		if mur_Ray1_Bool:
			if not(ground_Ray):
				player.motion.x += MURAL_X
			mur_Ray2.enabled = false
			mur_Ray1.enabled = true
		elif mur_Ray2_Bool:
			if not(ground_Ray):
				player.motion.x -= MURAL_X
			mur_Ray1.enabled = false
			mur_Ray2.enabled = true

			
func _ready():
	sautMuralPermis = Global.DrapeauSautMural
	player = get_parent().get_parent()
	sons = player.get_node("BruitagesPerso")
	mur_Ray1 = player.get_node("murRay1_saut")
	mur_Ray2 = player.get_node("murRay2_saut")
	ground_Ray1 = player.get_node("groundRay1")
	ground_Ray2 = player.get_node("groundRay2")
	PermisGauche=player.Permis_gauche
	PermisDroite=player.Permis_droite

func _process(delta):
	InputOK = player.InputOK
	mur_Ray1_Bool = mur_Ray2.is_colliding()
	mur_Ray2_Bool = mur_Ray1.is_colliding()
	
	ground_Ray1_Bool = ground_Ray2.is_colliding()
	ground_Ray2_Bool = ground_Ray1.is_colliding()
	ground_Ray = ground_Ray1_Bool or ground_Ray2_Bool
	# direction du joueur
	if mur_Ray2_Bool:
		right = -1
	elif mur_Ray1_Bool:
		right = 1
	
	# MAJ cool_down
	if (temps_saut > 0):
		#print("MAJ timer")
		temps_saut -= delta
		
	if (delai > 0):
		#print("MAJ timer")
		delai -= delta
	else:
		mur_Ray1.enabled = true
		mur_Ray2.enabled = true
		
	if(temps_dash > 0):
		#print("MAJ timer")
		temps_dash -= delta
		
	if(timer_saut > 0):
		#print("MAJ timer")
		timer_saut -= delta
		
	if(timer_saut <= 0) or ground_Ray:
		mur_Ray1.enabled = true
		mur_Ray2.enabled = true
		player.walljump = false

	# MAJ de la position
	if(temps_dash > 0):
		player.air_motion = 0 #DASH_LENGTH*right
	else : 
		player.air_motion = 0
	
	if(temps_saut > 0):
		player.motion.y=- JUMP * FACTEUR_ATTENUATION
		if right == 1:
			PermisGauche = false
			PermisDroite = true
		else :
			PermisGauche = true
			PermisDroite = false
	else :
		PermisGauche = true
		PermisDroite = true


func _on_airePourObjet_area_entered(area):
	if area.is_in_group("saut_mur"):
		sautMuralPermis = true
		player.sautMuralPermis = true
		Global.DrapeauSautMural = true
