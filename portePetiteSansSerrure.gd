extends Area2D

# Declare member variables here. Examples:
# var a = 2



func _on_porteSansSerrure_body_exited(body):
	if body.name == "player":
		if Global.DrapeauCle:
			get_tree().change_scene("res://environnementSalleIntermédiaireVide.tscn")
		else:
			get_tree().change_scene("res://environnementSalleIntermédiaire.tscn")


func _on_porte2_body_entered(body):
	if body.name == "player":
		# entrer le nom de la nouvelle scene
		var t = Timer.new()
		t.set_wait_time(0.5)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
		if Global.DrapeauCle:
			get_tree().change_scene("res://environnementSalleIntermédiaireVide.tscn")
		else:
			get_tree().change_scene("res://environnementSalleIntermédiaire.tscn")
